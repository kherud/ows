from pymongo.errors import DuplicateKeyError
from register_player import parse_player
from pymongo import MongoClient
import argparse


def calculate_game(args, old_profile, updated_profile, old_game_index):
    won = updated_profile["ALL HEROES"]["Game"]["Games Won"] - old_profile["ALL HEROES"]["Game"]["Games Won"]
    lost = updated_profile["ALL HEROES"]["Game"]["Games Lost"] - old_profile["ALL HEROES"]["Game"]["Games Lost"]
    win = (1 + won - lost) / 2  # considering a tie
    game = {
        "battle_tag": old_profile["battle_tag"],
        "game_index": old_game_index + 1,
        "sr_change": updated_profile["rank"] - old_profile["rank"],
        "win": win,
        "stats": {}
    }
    if args.m:
        game["map"] = input("Enter the map that was lastly played: ")

    for hero, stats in updated_profile.items():
        if type(stats) is dict:  # ignore non dicitonary attributes, e.g. battle_tag
            hero_stats = get_hero_stats(old_profile, stats, hero)
            if hero_stats is not None:  # if hero was played
                game["stats"][hero] = hero_stats
    return game


def get_hero_stats(old_profile, updated_stats, hero):
    if hero not in old_profile:
        return updated_stats
    if updated_stats["Game"]["Games Played"] - old_profile[hero]["Game"]["Games Played"] < 1:
        return None
    hero_stats = {}
    relevant_categories = ["Combat", "Assists", "Match Awards", "Miscellaneous", "Hero Specific"]
    for category in relevant_categories:
        if category not in updated_stats:
            continue
        category_stats = {}
        for key, value in updated_stats[category].items():
            category_stats[key] = value - old_profile[hero][category][key] if key in old_profile[hero][category] else value
        hero_stats[category] = category_stats
    return hero_stats


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Select manual options.')
    parser.add_argument("-m", action="store_true",
                        help="Manually select a map that was played.")
    args = parser.parse_args()

    client = MongoClient("localhost", 27017)
    db = client.overwatch
    profile_col = db.profiles
    history_col = db.history
    history_col.create_index([("game_index", 1), ("battle_tag", 1)], unique=True)

    for profile in profile_col.find():
        game_index = profile["ALL HEROES"]["Game"]["Games Played"]
        updated_profile = parse_player(profile["battle_tag"])
        games_played = updated_profile["ALL HEROES"]["Game"]["Games Played"] - game_index
        if games_played == 0:
            print("No updates for profile '{}' monitored -- {} games played so far.".format(profile["battle_tag"], game_index))
        elif games_played > 1:
            print("Monitored {} new games for profile '{}', only updating profile.".format(games_played, profile["battle_tag"]))
            profile_col.update({"battle_tag": profile["battle_tag"]}, updated_profile)
        else:
            game = calculate_game(args, profile, updated_profile, game_index)
            try:
                history_col.insert_one(game)
                print("Successfully updated match history for profile '{}'.".format(profile["battle_tag"]))
            except DuplicateKeyError:
                print("Game already in database -- error highly possible.")
            profile_col.update({"battle_tag": profile["battle_tag"]}, updated_profile, True)